﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using HologramSpriteManager;
using Newtonsoft.Json;
using Orbit_Animation_Editor.Classes;
using Orbit_Animation_Editor.Properties;

namespace Orbit_Animation_Editor.Forms
{
    public partial class MyForm : Form
    {
        private const int MaxSize = 200;

        public AnimationEditor AnimEditor;
        private AnimatedSpriteSequences _meta;
        private BindingList<AnimationSequence> _currentSequenceList;
        private bool _bIsFirstClickOnPictureBox = true;


        public MyForm()
        {
            InitializeComponent();

            PopulateTypeCombobox();
            _meta = new AnimatedSpriteSequences {AnimationSequences = new List<AnimationSequence>()};
            _currentSequenceList = new BindingList<AnimationSequence>();

            var pbLogo = new PictureBox
            {
                Size = new Size(430, 70),
                Location = new Point(5, 40),
                Image = Resources.Hologram
            };

            Controls.Add(pbLogo);
        }

        private void PopulateTypeCombobox()
        {
            for (int i = 0; i < Enum.GetNames(typeof(AnimationType)).Length; i++)
            {
                cmbType.Items.Add((AnimationType)i);
            }
        }

        private void PopulateForm(AnimatedSpriteSequences meta)
        {
            txtDescription.Text = meta.Description;
            cmbType.SelectedText = meta.sType;

            foreach (var item in _currentSequenceList)
            {
                lbSequences.Items.Add(item.AnimationName);
            }

            foreach (var item in meta.SpriteMaps)
            {
                if (!StaticValues.SpriteMaps.Exists(x => x.Equals(item)))
                {
                    StaticValues.SpriteMaps.Add(item);
                }
                lbSpriteMaps.Items.Add(item.SpriteMapName);
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            AnimationSequence animSeqCurr = PopulateAnimationSequence(txtAnimLoop.Text, txtAnimName.Text, txtAnimTime.Text, dgvAnimationFrames);

            SetViewWindowSize(animSeqCurr);
            PrepareAnimatedSpriteToDraw(animSeqCurr);
            //_frmMono.Show();
        }

        private void PrepareAnimatedSpriteToDraw(AnimationSequence animSeqCurr)
        {
            AnimatedSpriteSequences sequences = new AnimatedSpriteSequences
            {
                AnimationSequences = new List<AnimationSequence> {animSeqCurr},
                SpriteMaps = new List<SpriteMap>()
            };

            foreach (var item in animSeqCurr.AnimationFrames)
            {
                if (!sequences.SpriteMaps.Exists(x => x.SpriteMapName == StaticValues.SpriteMaps[item.map].SpriteMapName))
                {
                    sequences.SpriteMaps.Add(StaticValues.SpriteMaps[item.map]);
                }
            }

            animEditor.ChangeCurrentSprite(new AnimatedSprite(ass, false));
			}

        private void SetViewWindowSize(AnimationSequence animSeqCurr)
        {
            SpriteMap smCurr = StaticValues.SpriteMaps[animSeqCurr.GetCurrentFrameMeta().map];
            Image image = PopulateTexture(smCurr.SpriteMapName);
            int width = image.Width / smCurr.Columns;
            int height = image.Height / smCurr.Rows;
            //_frmMono.pictureBox1.Size = new Size(width < 800 ? 800 : width, height < 600 ? 600 : height);
        }

        private AnimationSequence PopulateAnimationSequence(string sLoop, string sName, string sTime, DataGridView dgvCurr)
        {
            try
            {
                AnimationSequence animCurr = new AnimationSequence
                {
                    AnimationLoop = int.Parse(sLoop),
                    AnimationName = sName,
                    AnimationTime = int.Parse(sTime),
                    AnimationFrames = new List<AnimationFrame>()
                };

                for (var i = 0; i < dgvCurr.Rows.Count - 1; i++)
                {
                    AnimationFrame animFrameCurr = new AnimationFrame
                    {
                        map = (int) dgvCurr.Rows[i].Cells["map"].Value,
                        row = (int) dgvCurr.Rows[i].Cells["row"].Value,
                        column = (int) dgvCurr.Rows[i].Cells["column"].Value,
                        weight = (int) dgvCurr.Rows[i].Cells["weight"].Value
                    };
                    animCurr.AnimationFrames.Add(animFrameCurr);
                }
                animCurr.CalcWeight();

                return animCurr;
            }
            catch (NullReferenceException)
            {
                MessageBox.Show(Resources.MyForm_PopulateAnimationSequence_Please_check_for_empty_sequence_inputs_);
                return null;
            }
            catch (FormatException)
            {
                MessageBox.Show(Resources.MyForm_PopulateAnimationSequence_Please_check_sequence_inputs_);
                return null;
            }
        }

        private void lbSpriteMaps_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbSpriteMaps.SelectedIndex == -1) return;

            if (panelSpriteImage.Controls.Count > 0)
            {
                panelSpriteImage.Controls.Clear();
            }

            SpriteMap smCurr = StaticValues.SpriteMaps[lbSpriteMaps.SelectedIndex];
            Image imgSprite = PopulateTexture(smCurr.SpriteMapName);

            for (int i = 0; i < smCurr.Rows; i++)
            {
                for (int j = 0; j < smCurr.Columns; j++)
                {
                    panelSpriteImage.Controls.Add(CreatePictureBox(smCurr, imgSprite, i, j));
                }
            }
        }

        private MyPictureBox CreatePictureBox(SpriteMap smCurr, Image imgSprite, int i, int j)
        {
            var rectCurr = smCurr.GetRectangle(i, j);

            int pbWidth = rectCurr.Width;
            int pbHeight = rectCurr.Height;

            if (rectCurr.Width > MaxSize)
            {
                double asd = rectCurr.Width / (double)MaxSize;
                pbWidth = MaxSize;
                pbHeight = (int)(rectCurr.Height / asd);
            }
            else if (rectCurr.Height > MaxSize)
            {
                double asd = rectCurr.Height / (double)MaxSize;
                pbHeight = MaxSize;
                pbWidth = (int)(rectCurr.Height / asd);
            }

            MyPictureBox pbCurr = new MyPictureBox(i, j, pbWidth, pbHeight)
            {
                Image = CropImage(imgSprite as Bitmap, new Rectangle(rectCurr.X, rectCurr.Y, rectCurr.Width, rectCurr.Height))
            };

            pbCurr.Click += Picturebox_Click;
            return pbCurr;
        }

        void Picturebox_Click(object sender, EventArgs e)
        {
            MyPictureBox pbCurr = sender as MyPictureBox;
            if (pbCurr != null)
            {
                pbCurr.Invalidate();
                pbCurr.Paint += (send, evt) =>
                {
                    Random randonGen = new Random();
                    Color randomColor = Color.FromArgb(randonGen.Next(255), randonGen.Next(255), randonGen.Next(255));
                    evt.Graphics.DrawRectangle(new Pen(randomColor, 4), pbCurr.DisplayRectangle);
                };

                BindingList<AnimationFrame> animFrameCurr = (BindingList<AnimationFrame>)dgvAnimationFrames.DataSource;
            
                animFrameCurr.Add(new AnimationFrame() { column = pbCurr.Column, map = lbSpriteMaps.SelectedIndex, row = pbCurr.Row, weight = 10 } );
                if (_bIsFirstClickOnPictureBox)
                {
                    animFrameCurr.RemoveAt(0);
                    _bIsFirstClickOnPictureBox = false;
                }
            }
        }

        private Image CropImage(Image img, Rectangle cropArea)
        {
            Bitmap target = new Bitmap(cropArea.Width, cropArea.Height);

            using(Graphics g = Graphics.FromImage(target))
            {
                g.DrawImage(img, new Rectangle(0, 0, target.Width, target.Height), cropArea, GraphicsUnit.Pixel);
            }

            return target;
        }

        public Image PopulateTexture(string spriteName)
        {
            using (FileStream fileStream = new FileStream(spriteName + ".png", FileMode.Open))
            {
                return Image.FromStream(fileStream);
            }
        }

        private void btnLoadImages_Click(object sender, EventArgs e)
        {
            FrmCreateSpriteMap newForm = new FrmCreateSpriteMap(this);
            newForm.ShowDialog();
        }

        public void UpdateSpriteMapListbox()
        {
            lbSpriteMaps.Items.Clear();
            foreach (var item in StaticValues.SpriteMaps)
            {
                lbSpriteMaps.Items.Add(item.SpriteMapName);
            }
        }

        private void lbSequences_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbSequences.SelectedIndex == -1) return;

            AnimationSequence animSquenceCurr = _currentSequenceList[lbSequences.SelectedIndex];
            txtAnimName.Text = animSquenceCurr.AnimationName;
            txtAnimLoop.Text = animSquenceCurr.AnimationLoop.ToString();
            txtAnimTime.Text = animSquenceCurr.AnimationTime.ToString();

            BindingList<AnimationFrame> liCurr = new BindingList<AnimationFrame>();
            foreach (var item in animSquenceCurr.AnimationFrames)
            {
                liCurr.Add(item);
            }
            dgvAnimationFrames.DataSource = liCurr;
        }

        private void ClearAnimSequencePanel()
        {
            AnimEditor.ChangeCurrentSprite(null);
            txtAnimLoop.Text = "";
            txtAnimName.Text = "";
            txtAnimTime.Text = "";
            _bIsFirstClickOnPictureBox = true;
            if (dgvAnimationFrames.DataSource != null)
            {
                dgvAnimationFrames.DataSource = null;
            }            
        }

        private void ClearAllForm()
        {
            txtDescription.Text = "";
            cmbType.Items.Clear();
            cmbType.Text = "";
            StaticValues.SpriteMaps = new List<SpriteMap>();
            _currentSequenceList = new BindingList<AnimationSequence>();
            lbSequences.Items.Clear();
            lbSpriteMaps.Items.Clear();
            panelSpriteImage.Controls.Clear();
            ClearAnimSequencePanel();
        }

        #region Tool Strip Functions
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog
            {
                Filter = Resources.MyForm_loadToolStripMenuItem_Click_JSon_Files____json____json
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (ofd.FileName != "" || ofd.FileName != String.Empty)
                {
                    StaticValues.CurrentFilePath = ofd.FileName;

                    if (ofd.FileName != null)
                    {
                        string text = File.ReadAllText(ofd.FileName);
                        _meta = JsonConvert.DeserializeObject<AnimatedSpriteSequences>(text);
                    }

                    newToolStripMenuItem_Click(null, null);
                    ConvertSequenceLitToBindingList(_meta.AnimationSequences);
                    if (_meta != null)
                    {
                        meta.PopulateSequence(true, false);
                        PopulateForm(meta);
                    }
                }
            }
        }

        private void ConvertSequenceLitToBindingList(List<AnimationSequence> list)
        {
            foreach (var item in list)
            {
                _currentSequenceList.Add(item);
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearAllForm();
            panel1.Visible = true;
            PopulateTypeCombobox();
            BindingList<AnimationFrame> animFrameCurr = new BindingList<AnimationFrame> {new AnimationFrame()};
            dgvAnimationFrames.DataSource = animFrameCurr;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnimatedSpriteSequences animCurr = PopulateSequenceToSave();

            String sJson = JsonConvert.SerializeObject(animCurr, Formatting.None);
            String sCurrentPath = StaticValues.AnimationPath + "\\" + animCurr.Description;
            string sCurrentSpritesPath = sCurrentPath + "\\Sprites";

            CreateDirectoryIfNotExists(sCurrentPath, sCurrentSpritesPath);

            using (TextWriter writer = File.CreateText(sCurrentPath + "\\" + animCurr.Description + ".json"))
            {
                writer.Write(sJson);
            }

            foreach (var item in animCurr.SpriteMaps)
            {
                var sCurr = sCurrentPath + "\\" + item.SpriteMapName.Split('/')[0] + "\\" + item.SpriteMapName.Split('/')[1] + ".png";
                if (!File.Exists(sCurr))
                {
                    File.Copy(item.SpriteMapName + ".png", sCurrentPath + "\\" + item.SpriteMapName + ".png");
                }
            }
        }

        private static void CreateDirectoryIfNotExists(String sCurrentPath, string sCurrentSpritesPath)
        {
            if (!Directory.Exists(sCurrentPath))
            {
                Directory.CreateDirectory(sCurrentPath);
            }

            if (!Directory.Exists(sCurrentSpritesPath))
            {
                Directory.CreateDirectory(sCurrentSpritesPath);
            }
        }

        private AnimatedSpriteSequences PopulateSequenceToSave()
        {
            AnimatedSpriteSequences animCurr = new AnimatedSpriteSequences
            {
                Description = txtDescription.Text,
                sType = cmbType.SelectedIndex != -1 ? Enum.GetNames(typeof (AnimationType))[cmbType.SelectedIndex] : cmbType.Text,
                SpriteMaps = StaticValues.SpriteMaps,
                AnimationSequences = new List<AnimationSequence>()
            };

            foreach (var item in _currentSequenceList)
            {
                animCurr.AnimationSequences.Add(item);
            }

            return animCurr;
        }

        #endregion
        
        private void btnSaveSequence_Click(object sender, EventArgs e)
        {
            AnimationSequence animCurr = PopulateAnimationSequence(txtAnimLoop.Text, txtAnimName.Text, txtAnimTime.Text, dgvAnimationFrames);
            if (animCurr == null)
            {
                return;
            }
            _currentSequenceList.Add(animCurr);
            lbSequences.Items.Add(animCurr.AnimationName);
            ClearAnimSequencePanel();
        }

        private void btnNewSequence_Click(object sender, EventArgs e)
        {
            ClearAnimSequencePanel();
            BindingList<AnimationFrame> animFrameCurr = new BindingList<AnimationFrame> {new AnimationFrame()};
            dgvAnimationFrames.DataSource = animFrameCurr;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void myForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            AnimEditor.Exit();
        }

        private void backgorundColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();

            DialogResult result = cd.ShowDialog();
            if (result == DialogResult.OK)
            {
                // Set form background to the selected color.
                AnimEditor.ChangeBackgroundColor(cd.Color.R, cd.Color.G, cd.Color.B);
            }
        }

    }
}
