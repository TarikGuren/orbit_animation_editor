﻿namespace Orbit_Animation_Editor.Forms
{
    partial class MyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyForm));
            this.btnShowSequence = new System.Windows.Forms.Button();
            this.btnLoadImages = new System.Windows.Forms.Button();
            this.lbSpriteMaps = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelSpriteImage = new System.Windows.Forms.Panel();
            this.lbSequences = new System.Windows.Forms.ListBox();
            this.btnNewSequence = new System.Windows.Forms.Button();
            this.btnSaveSequence = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.dgvAnimationFrames = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAnimLoop = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAnimTime = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAnimName = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgorundColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnimationFrames)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnShowSequence
            // 
            this.btnShowSequence.Location = new System.Drawing.Point(639, 295);
            this.btnShowSequence.Name = "btnShowSequence";
            this.btnShowSequence.Size = new System.Drawing.Size(87, 36);
            this.btnShowSequence.TabIndex = 2;
            this.btnShowSequence.Text = "Show";
            this.btnShowSequence.UseVisualStyleBackColor = true;
            this.btnShowSequence.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnLoadImages
            // 
            this.btnLoadImages.Location = new System.Drawing.Point(605, 14);
            this.btnLoadImages.Name = "btnLoadImages";
            this.btnLoadImages.Size = new System.Drawing.Size(140, 28);
            this.btnLoadImages.TabIndex = 3;
            this.btnLoadImages.Text = "Create Sprite Map";
            this.btnLoadImages.UseVisualStyleBackColor = true;
            this.btnLoadImages.Click += new System.EventHandler(this.btnLoadImages_Click);
            // 
            // lbSpriteMaps
            // 
            this.lbSpriteMaps.FormattingEnabled = true;
            this.lbSpriteMaps.ItemHeight = 16;
            this.lbSpriteMaps.Location = new System.Drawing.Point(605, 64);
            this.lbSpriteMaps.Name = "lbSpriteMaps";
            this.lbSpriteMaps.Size = new System.Drawing.Size(140, 132);
            this.lbSpriteMaps.TabIndex = 9;
            this.lbSpriteMaps.SelectedIndexChanged += new System.EventHandler(this.lbSpriteMaps_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.panelSpriteImage);
            this.panel1.Controls.Add(this.lbSequences);
            this.panel1.Controls.Add(this.btnNewSequence);
            this.panel1.Controls.Add(this.lbSpriteMaps);
            this.panel1.Controls.Add(this.btnSaveSequence);
            this.panel1.Controls.Add(this.btnLoadImages);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cmbType);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtDescription);
            this.panel1.Controls.Add(this.dgvAnimationFrames);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtAnimLoop);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtAnimTime);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtAnimName);
            this.panel1.Controls.Add(this.btnShowSequence);
            this.panel1.Location = new System.Drawing.Point(16, 44);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(1058, 505);
            this.panel1.TabIndex = 13;
            this.panel1.Visible = false;
            // 
            // panelSpriteImage
            // 
            this.panelSpriteImage.AutoScroll = true;
            this.panelSpriteImage.AutoSize = true;
            this.panelSpriteImage.Location = new System.Drawing.Point(759, 14);
            this.panelSpriteImage.Name = "panelSpriteImage";
            this.panelSpriteImage.Size = new System.Drawing.Size(291, 298);
            this.panelSpriteImage.TabIndex = 22;
            // 
            // lbSequences
            // 
            this.lbSequences.FormattingEnabled = true;
            this.lbSequences.ItemHeight = 16;
            this.lbSequences.Location = new System.Drawing.Point(163, 64);
            this.lbSequences.Name = "lbSequences";
            this.lbSequences.Size = new System.Drawing.Size(120, 116);
            this.lbSequences.TabIndex = 28;
            this.lbSequences.SelectedIndexChanged += new System.EventHandler(this.lbSequences_SelectedIndexChanged);
            // 
            // btnNewSequence
            // 
            this.btnNewSequence.Location = new System.Drawing.Point(289, 64);
            this.btnNewSequence.Name = "btnNewSequence";
            this.btnNewSequence.Size = new System.Drawing.Size(69, 35);
            this.btnNewSequence.TabIndex = 26;
            this.btnNewSequence.Text = "New";
            this.btnNewSequence.UseVisualStyleBackColor = true;
            this.btnNewSequence.Click += new System.EventHandler(this.btnNewSequence_Click);
            // 
            // btnSaveSequence
            // 
            this.btnSaveSequence.Location = new System.Drawing.Point(368, 63);
            this.btnSaveSequence.Name = "btnSaveSequence";
            this.btnSaveSequence.Size = new System.Drawing.Size(87, 36);
            this.btnSaveSequence.TabIndex = 27;
            this.btnSaveSequence.Text = "Save";
            this.btnSaveSequence.UseVisualStyleBackColor = true;
            this.btnSaveSequence.Click += new System.EventHandler(this.btnSaveSequence_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 17);
            this.label3.TabIndex = 25;
            this.label3.Text = "Animation Sequences:";
            // 
            // cmbType
            // 
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(349, 8);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(121, 24);
            this.cmbType.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(299, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 23;
            this.label2.Text = "Type:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 22;
            this.label1.Text = "Description:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(98, 11);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(179, 22);
            this.txtDescription.TabIndex = 21;
            // 
            // dgvAnimationFrames
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAnimationFrames.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAnimationFrames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAnimationFrames.Location = new System.Drawing.Point(8, 295);
            this.dgvAnimationFrames.Name = "dgvAnimationFrames";
            this.dgvAnimationFrames.RowTemplate.Height = 24;
            this.dgvAnimationFrames.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAnimationFrames.Size = new System.Drawing.Size(590, 202);
            this.dgvAnimationFrames.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Animation Loop:";
            // 
            // txtAnimLoop
            // 
            this.txtAnimLoop.Location = new System.Drawing.Point(126, 248);
            this.txtAnimLoop.Name = "txtAnimLoop";
            this.txtAnimLoop.Size = new System.Drawing.Size(137, 22);
            this.txtAnimLoop.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 223);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Animation Time:";
            // 
            // txtAnimTime
            // 
            this.txtAnimTime.Location = new System.Drawing.Point(126, 220);
            this.txtAnimTime.Name = "txtAnimTime";
            this.txtAnimTime.Size = new System.Drawing.Size(137, 22);
            this.txtAnimTime.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 195);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Animation Name:";
            // 
            // txtAnimName
            // 
            this.txtAnimName.Location = new System.Drawing.Point(126, 192);
            this.txtAnimName.Name = "txtAnimName";
            this.txtAnimName.Size = new System.Drawing.Size(137, 22);
            this.txtAnimName.TabIndex = 7;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1090, 28);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backgorundColorToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // backgorundColorToolStripMenuItem
            // 
            this.backgorundColorToolStripMenuItem.Name = "backgorundColorToolStripMenuItem";
            this.backgorundColorToolStripMenuItem.Size = new System.Drawing.Size(197, 24);
            this.backgorundColorToolStripMenuItem.Text = "Backgorund Color";
            this.backgorundColorToolStripMenuItem.Click += new System.EventHandler(this.backgorundColorToolStripMenuItem_Click);
            // 
            // MyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1090, 564);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(460, 200);
            this.Name = "MyForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Sprite Map Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.myForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.myForm_FormClosed);
            this.Load += new System.EventHandler(this.myForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnimationFrames)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnShowSequence;
        private System.Windows.Forms.Button btnLoadImages;
        private System.Windows.Forms.ListBox lbSpriteMaps;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAnimLoop;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAnimTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAnimName;
        private System.Windows.Forms.DataGridView dgvAnimationFrames;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ListBox lbSequences;
        private System.Windows.Forms.Button btnNewSequence;
        private System.Windows.Forms.Button btnSaveSequence;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Panel panelSpriteImage;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backgorundColorToolStripMenuItem;

    }
}