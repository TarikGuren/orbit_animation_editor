﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Orbit_Animation_Editor.Forms
{
    public partial class MonoPlayer : Form
    {
        public AnimationEditor animEditor;
        public MonoPlayer()
        {
            InitializeComponent();
        }

        internal IntPtr getDrawSurface()
        {
            return pictureBox1.Handle;
        }

        internal void CreateMainForm()
        {
            myForm form = new myForm(this);
            form.Show();
        }

        private void backgroundColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();

            DialogResult dr = cd.ShowDialog();

            if (dr == DialogResult.OK)
            {
                animEditor.ChangeBackgroundColor(cd.Color.R, cd.Color.G, cd.Color.B);
            }
        }

    }
}
