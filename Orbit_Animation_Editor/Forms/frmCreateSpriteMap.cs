﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using HologramSpriteManager;
using Orbit_Animation_Editor.Classes;
using Orbit_Animation_Editor.Properties;

namespace Orbit_Animation_Editor.Forms
{
    public partial class FrmCreateSpriteMap : Form
    {
        readonly MyForm _frmParent;
        public FrmCreateSpriteMap(MyForm parentForm)
        {
            InitializeComponent();
            _frmParent = parentForm;
        }

        string _sFilePath;
        private void frmCreateSpriteMap_Load(object sender, EventArgs e)
        {
            
        }
        public Image PopulateTexture(string spritePath)
        {
            if (!File.Exists("Sprites/" + GetSpriteName(spritePath) + ".png"))
            {
                File.Copy(spritePath, "Sprites/" + GetSpriteName(spritePath) + ".png");
            }
            using (var fileStream = new FileStream(spritePath, FileMode.Open))
            {
                return Image.FromStream(fileStream);
            }
        }

        private void btnLoadImage_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog {Filter = Resources.FrmCreateSpriteMap_btnLoadImage_Click_PNG_Files____png____png};

            if (ofd.ShowDialog() != DialogResult.OK) return;

            if (!ofd.CheckFileExists && ofd.FileName == null && ofd.FileName == "") return;

            _sFilePath = ofd.FileName;
            pbImage.Image = PopulateTexture(_sFilePath);
        }

        private static string GetSpriteName(string spritePath)
        {
            var sPath = spritePath.Split('\\');
            var sCurr = sPath[sPath.Length - 1];
            sCurr = sCurr.Substring(0, sCurr.Length - 4);
            return sCurr;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var smCurr = new SpriteMap
            {
                SpriteMapName = "Sprites/" + GetSpriteName(_sFilePath),
                Rows = Convert.ToInt32(txtRow.Text),
                Columns = Convert.ToInt32(txtColumn.Text)
            };

            smCurr.PopulateTexture(true);
            StaticValues.SpriteMaps.Add(smCurr);

            _frmParent.UpdateSpriteMapListbox();
            Close();
        }

    }
}
