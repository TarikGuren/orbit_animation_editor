﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Common;
using FarseerPhysics;


namespace HologramSpriteManager
{

    class AnimatedSprite
    {
        //setup
        public AnimatedSpriteSequences Sequences;

        public bool bAddPhysics;
        public Vector2 Position;
        public Body PhysicsBody;
        public Vector2 PhysicsOrigin;
        public bool Scrolling = false;

        public Vector2 Movement { get; set; }

        public AnimatedSprite Clone()
        {
            AnimatedSprite ret = new AnimatedSprite();
            ret.Sequences = Sequences.Clone();
            return ret;
        }


        public AnimatedSprite(AnimatedSpriteSequences Meta, bool _bAddPhysics)
        {
            Sequences = Meta;
            bAddPhysics = _bAddPhysics;
        }
        public AnimatedSprite()
        {
        }

        public void ChangeAnimation(string sAnim)
        { 
            Sequences.SetAnimation(sAnim);

        }
        public void ChangeAnimation(string sAnim,bool bForceRestart)
        {
            Sequences.SetAnimation(sAnim,bForceRestart);

        }
        public void ChangeAnimation(string sAnim,bool bForceRestart,bool bFlow)
        {
            Sequences.SetAnimationFlow(sAnim,bForceRestart);

        }

		/// <summary>
		/// Changes the animation.
		/// </summary>
		/// <param name="startAnim">Start animation.</param>
		/// <param name="endAnim">End animation.</param>
		/// <param name="duration">Duration in miliseconds.Default value is 0. If not set, it'll use the duration from json.</param>
		public void ChangeAnimation(string startAnim,string endAnim,int duration = 0)
		{
			Sequences.SetAnimation(startAnim,endAnim,duration);
		}

        SpriteEffects ActiveEffect = SpriteEffects.None;
        public void SetEffect(SpriteEffects Effect)
        {
            ActiveEffect = Effect;
        }

        public void UpdatePhysics()
        {
            if (PhysicsBody != null)
            {
                PhysicsBody.Dispose();
            }
            Sequences.GetCurrentBody(out PhysicsBody, out PhysicsOrigin);

            Transform t;
            t.p = new Vector2(ConvertUnits.ToSimUnits(Position.X),ConvertUnits.ToSimUnits(Position.Y) );
            PhysicsBody.SetTransform(t.p, 0);
            PhysicsBody.Enabled = true;
            PhysicsBody.OnCollision += BodyCollide;

            
        }

        public virtual bool BodyCollide(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            // will be called whenever some other body collides with 'body'
            Console.WriteLine("Body1 Collide from base");
            return false;
        }

        public void Draw()
        {


            CurrentFrame frame = Sequences.GetCurrentFrame();
            //Console.WriteLine(frame.SourceRectangle);
            int X = (int)Position.X;
            int Y = (int)Position.Y;
            if (Scrolling)
            {
                X += SpriteManager.iXOffset;
                Y += SpriteManager.iYOffset;
            }
            Rectangle destinationRectangle = new Rectangle(X,Y, frame.SourceRectangle.Width, frame.SourceRectangle.Height);
            if (bAddPhysics)
            {
                Transform t;
                PhysicsBody.GetTransform(out t);
                Rectangle DestinationRectangle = new Rectangle((int)ConvertUnits.ToDisplayUnits(t.p.X), (int)ConvertUnits.ToDisplayUnits(t.p.Y), frame.SourceRectangle.Width, frame.SourceRectangle.Height);
                SpriteManager.spriteBatch.Draw(frame.SpriteMapTexture, DestinationRectangle, frame.SourceRectangle, Color.White, 0f, PhysicsOrigin, SpriteEffects.None, 0f);

            }
            else
            {
                SpriteManager.spriteBatch.Draw(frame.SpriteMapTexture, destinationRectangle, frame.SourceRectangle, Color.White, 0f, new Vector2(), ActiveEffect, 0);
            }
        }

		public void DrawWithAlpha(Color color)
		{
			CurrentFrame frame = Sequences.GetCurrentFrame();
			//Console.WriteLine(frame.SourceRectangle);
            int X = (int)Position.X;
            int Y = (int)Position.Y;
            if (Scrolling)
            {
                X += SpriteManager.iXOffset;
                Y += SpriteManager.iYOffset;
            }
			Rectangle destinationRectangle = new Rectangle(X, Y, frame.SourceRectangle.Width, frame.SourceRectangle.Height);
			SpriteManager.spriteBatch.Draw(frame.SpriteMapTexture, destinationRectangle, frame.SourceRectangle, color);

		}

        public Rectangle Bounds
        {
            get
            {
                CurrentFrame current = Sequences.GetCurrentFrame();
                return new Rectangle((int)Position.X, (int)Position.Y, current.SourceRectangle.Width, current.SourceRectangle.Height);
                         // width, height);
            }
        }

    }
}
