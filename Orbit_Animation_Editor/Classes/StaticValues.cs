﻿using System;
using System.Collections.Generic;
using HologramSpriteManager;

namespace Orbit_Animation_Editor.Classes
{
    public static class StaticValues
    {
        public static string CurrentFilePath;
        public static List<SpriteMap> SpriteMaps = new List<SpriteMap>();

        public static string AnimationPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Animations";
    }
}
