﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Orbit_Animation_Editor.Classes
{
    class Camera2D    
    {
        private Rectangle _bounds;
        private float _zoom = 1f;
        private Vector2 _center;

        #region Properties
        public Viewport Viewport { get; set; }
        public Vector2 Center 
        { 
            get { return _center; } 
            set { _center = value; }
        }
        public Matrix Transform { get; private set; }

        public float Rotation { get; private set; }

        public Rectangle Bounds 
        {
            get { return _bounds; }
        }
        public float Zoom
        {
            get { return _zoom; }
            set
            {
                _zoom = value;
                if (_zoom < 0.1f)
                {
                    _zoom = 0.1f;
                }
                else if (_zoom > 2f)
                {
                    _zoom = 2f;
                }
            }
        }

        #endregion


        public Camera2D(Viewport viewport)
        {
            Viewport = viewport;
            _bounds = new Rectangle(0,0,1,1);
        }

        public void Update(AnimatedSprite sprite)
        {
            SetBoundsRectangle(sprite);
            //SetCenterPoint(sprite);
            SetTransformMatrix();

            //Console.WriteLine(_transform.ToString());
        }

        private void SetTransformMatrix()
        {
            var scaleMatrix = Matrix.CreateScale(new Vector3(_zoom, _zoom, 1));
            var translationMatrix = Matrix.CreateTranslation(-1 * _center.X, -1 * _center.Y, 0);
            Transform = translationMatrix * scaleMatrix; 
            //Matrix.Multiply(ref translationMatrix, ref scaleMatrix, out _transform);
        }

        private void SetBoundsRectangle(AnimatedSprite sprite)
        {
            _bounds.X = (int)(sprite.Position.X - Viewport.Width / 2f / _zoom);
            _bounds.Y = (int)(sprite.Position.Y - Viewport.Height / 2f / _zoom);
            _bounds.Width = (int)(Viewport.Width / _zoom);
            _bounds.Height = (int)(Viewport.Height / _zoom);
        }

        public void SetZoom(float fZoomValue)
        {
            Zoom += fZoomValue;
        }

        public void Rotate(float rotationvalue)
        {
            Rotation += rotationvalue;
        }

        public Vector2 ScreenToWorld(Vector2 mouseLocation)
        {
            return Vector2.Transform(mouseLocation, Matrix.Invert(Transform));
        }

        public Vector2 WorldToScreen(Vector2 mouseLocation)
        {
            return Vector2.Transform(mouseLocation, Transform);
        }
    }
}


