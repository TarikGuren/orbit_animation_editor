﻿#region Using Statements

using System;
using System.Windows.Forms;
using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Microsoft.Xna.Framework.Storage;
//using Microsoft.Xna.Framework.GamerServices;
using HologramSpriteManager;
using System.Linq;
using System.Windows.Forms;
using Orbit_Animation_Editor.Forms;
#endregion

namespace Orbit_Animation_Editor.Classes
{
    public class AnimationEditor : Game
    {
        readonly Control _gameForm;
        public Color BackgroundColor;
        GraphicsDeviceManager graphics;
        //SpriteBatch spriteBatch;

        IntPtr drawingSurface;
        System.Windows.Forms.Control gameForm;

        public Color backgroundColor = Color.Black;
        private Color contrastColor = Color.White;

        public AnimationSequence AnimCurrent;
        private AnimatedSprite _currentSprite;

        FontRenderer fr;

        Camera2D _camera;

        public AnimationEditor()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            SpriteManager.ContentShell = Content;
        }


        protected override void Initialize()
        {
            _camera = new Camera2D(GraphicsDevice.Viewport);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteManager.GameTime = 0;

            SpriteManager.spriteBatch = new SpriteBatch(GraphicsDevice);

            Texture2D test = SpriteManager.ContentShell.Load<Texture2D>("Font/fonttest_0");
            fr = new FontRenderer( "Font/fonttest" , 1);

        }

        protected override void UnloadContent() { }

        protected override void Update(GameTime gameTime)
        {
            SpriteManager.GameTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (_currentSprite != null)
            {
                _camera.Update(_currentSprite);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.U))
            {
                _camera.SetZoom(0.005f);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.J))
            {
                _camera.SetZoom(-0.005f);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                if (_currentSprite != null) _currentSprite.Position.Y += 3;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                if (_currentSprite != null) _currentSprite.Position.Y -= 3;
            }
            
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                if (_currentSprite != null) _currentSprite.Position.X -= 3;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                if (_currentSprite != null) _currentSprite.Position.X += 3;
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(BackgroundColor);

            if (_currentSprite != null)
            {
                graphics.PreferredBackBufferWidth = _currentSprite.Bounds.Width > 800 ? _currentSprite.Bounds.Width : 800;
                graphics.PreferredBackBufferHeight = _currentSprite.Bounds.Height > 600 ? _currentSprite.Bounds.Height : 600;
                graphics.ApplyChanges();
            }

            SpriteManager.spriteBatch.Begin(SpriteSortMode.Texture,
                                      BlendState.AlphaBlend,
                                      SamplerState.LinearClamp,
                                      DepthStencilState.Default,
                                      RasterizerState.CullNone,
                                      null,
                                      _camera.Transform);
            
            if (CurrentSprite != null) CurrentSprite.Draw();
            
            SpriteManager.spriteBatch.Begin();

            fr.DrawTextWithWidth(new Vector2(10, 5), 100, "Zoom: " + _camera.Zoom, 1, contrastColor);

            SpriteManager.spriteBatch.End();

            base.Draw(gameTime);
        }

        internal void ChangeCurrentSprite(AnimatedSprite asCurr)
        {
            _currentSprite = asCurr;

            if (asCurr == null) return;

            _currentSprite.Position = new Vector2(0, 0);
            if (GraphicsDevice != null)
                _camera.Center = new Vector2((_currentSprite.Position.X) * -1, (_currentSprite.Position.Y) * -1);
        }

        internal void ChangeBackgroundColor(byte p1, byte p2, byte p3)
        {
            backgroundColor = new Color(p1, p2, p3);
            contrastColor = getContrastColor(); 
        }

        private Color getContrastColor()
        {
            return new Color(255 - backgroundColor.R, 255 - backgroundColor.G, 255 - backgroundColor.B);
        }
    }
}
