﻿using System.Drawing;
using System.Windows.Forms;

namespace Orbit_Animation_Editor.Classes
{
    class MyPictureBox : PictureBox
    {
        public int Row { get; set; }
        public int Column { get; set; }

        public MyPictureBox(int iRow, int iColumn, int iWidth, int iHeight)
        {
            Row = iRow;
            Column = iColumn;
            Width = iWidth;
            Height = iHeight;

            Location = new Point(Column * Width, Row * Height);

            BorderStyle = BorderStyle.FixedSingle;
            SizeMode = PictureBoxSizeMode.Zoom;
        }
    }
}
